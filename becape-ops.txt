 
,
    {
      "Name": "VIVO T80 - 01",
      "FLAG": "vivo",
      "Payload": "GET wss://d1jcj8m073srwz.cloudfront.net HTTP/1.1[crlf]Host: d1jcj8m073srwz.cloudfront.net[crlf]Connection: Upgrade[crlf]Upgrade: websocket[crlf][crlf]",
      "SNI": "",
      "TlsIP": "",
      "ProxyIP": "52.85.213.48",
      "ProxyPort": "80",
      "Info": "Proxy"
    },
    {
      "Name": "VIVO T443 - 01",
      "FLAG": "vivo",
      "Payload": "GET wss://d1jcj8m073srwz.cloudfront.net HTTP/1.1[crlf]Host: d1jcj8m073srwz.cloudfront.net[crlf]Connection: Upgrade[crlf]Upgrade: websocket[crlf][crlf]",
      "SNI": "d1jcj8m073srwz.cloudfront.net",
      "TlsIP": "52.85.213.48",
      "ProxyIP": "52.85.213.48",
      "ProxyPort": "443",
      "Info": "Tlsws"
    },
    {
      "Name": "VIVO T80 - 02",
      "FLAG": "vivo",
      "Payload": "GET wss://d1jcj8m073srwz.cloudfront.net HTTP/1.1[crlf]Host: d1jcj8m073srwz.cloudfront.net[crlf]Connection: Upgrade[crlf]Upgrade: websocket[crlf][crlf]",
      "SNI": "",
      "TlsIP": "",
      "ProxyIP": "13.227.126.49",
      "ProxyPort": "80",
      "Info": "Proxy"
    },
    {
      "Name": "VIVO T443 - 02",
      "FLAG": "vivo",
      "Payload": "GET wss://d1jcj8m073srwz.cloudfront.net HTTP/1.1[crlf]Host: d1jcj8m073srwz.cloudfront.net[crlf]Connection: Upgrade[crlf]Upgrade: websocket[crlf][crlf]",
      "SNI": "d1jcj8m073srwz.cloudfront.net",
      "TlsIP": "13.227.126.49",
      "ProxyIP": "13.227.126.49",
      "ProxyPort": "443",
      "Info": "Tlsws"
    },
    {
      "Name": "VIVO T443 - 03",
      "FLAG": "vivo",
      "Payload": "GET wss://d1jcj8m073srwz.cloudfront.net HTTP/1.1[crlf]Host: d1jcj8m073srwz.cloudfront.net[crlf]Connection: Upgrade[crlf]Upgrade: websocket[crlf][crlf]",
      "SNI": "d1jcj8m073srwz.cloudfront.net",
      "TlsIP": "108.139.182.100",
      "ProxyIP": "108.139.182.100",
      "ProxyPort": "443",
      "Info": "Tlsws"
    },
    {
      "Name": "VIVO T80 - 04",
      "FLAG": "vivo",
      "Payload": "GET wss://d1jcj8m073srwz.cloudfront.net HTTP/1.1[crlf]Host: d1jcj8m073srwz.cloudfront.net[crlf]Connection: Upgrade[crlf]Upgrade: websocket[crlf][crlf]",
      "SNI": "",
      "TlsIP": "",
      "ProxyIP": "108.139.95.90",
      "ProxyPort": "80",
      "Info": "Proxy"
    },
    {
      "Name": "VIVO T443 - 04",
      "FLAG": "vivo",
      "Payload": "GET wss://d1jcj8m073srwz.cloudfront.net HTTP/1.1[crlf]Host: d1jcj8m073srwz.cloudfront.net[crlf]Connection: Upgrade[crlf]Upgrade: websocket[crlf][crlf]",
      "SNI": "d1jcj8m073srwz.cloudfront.net",
      "TlsIP": "108.139.95.90",
      "ProxyIP": "108.139.95.90",
      "ProxyPort": "443",
      "Info": "Tlsws"
    },
    {
      "Name": "VIVO T80 - 05",
      "FLAG": "vivo",
      "Payload": "GET wss://d1jcj8m073srwz.cloudfront.net HTTP/1.1[crlf]Host: d1jcj8m073srwz.cloudfront.net[crlf]Connection: Upgrade[crlf]Upgrade: websocket[crlf][crlf]",
      "SNI": "",
      "TlsIP": "",
      "ProxyIP": "108.139.166.109",
      "ProxyPort": "80",
      "Info": "Proxy"
    },
    {
      "Name": "VIVO T443 - 05",
      "FLAG": "vivo",
      "Payload": "GET wss://d1jcj8m073srwz.cloudfront.net HTTP/1.1[crlf]Host: d1jcj8m073srwz.cloudfront.net[crlf]Connection: Upgrade[crlf]Upgrade: websocket[crlf][crlf]",
      "SNI": "d1jcj8m073srwz.cloudfront.net",
      "TlsIP": "108.139.166.109",
      "ProxyIP": "108.139.166.109",
      "ProxyPort": "443",
      "Info": "Tlsws"
    },
    {
      "Name": "VIVO T80 - 06",
      "FLAG": "vivo",
      "Payload": "GET wss://d1jcj8m073srwz.cloudfront.net HTTP/1.1[crlf]Host: d1jcj8m073srwz.cloudfront.net[crlf]Connection: Upgrade[crlf]Upgrade: websocket[crlf][crlf]",
      "SNI": "",
      "TlsIP": "",
      "ProxyIP": "52.85.213.82",
      "ProxyPort": "80",
      "Info": "Proxy"
    },
    {
      "Name": "VIVO T443 - 07",
      "FLAG": "vivo",
      "Payload": "GET wss://d1jcj8m073srwz.cloudfront.net HTTP/1.1[crlf]Host: d1jcj8m073srwz.cloudfront.net[crlf]Connection: Upgrade[crlf]Upgrade: websocket[crlf][crlf]",
      "SNI": "d1jcj8m073srwz.cloudfront.net",
      "TlsIP": "108.139.166.72",
      "ProxyIP": "108.139.166.72",
      "ProxyPort": "443",
      "Info": "Tlsws"
    },
    {
      "Name": "VIVO T80 - 08",
      "FLAG": "vivo",
      "Payload": "GET wss://d1jcj8m073srwz.cloudfront.net HTTP/1.1[crlf]Host: d1jcj8m073srwz.cloudfront.net[crlf]Connection: Upgrade[crlf]Upgrade: websocket[crlf][crlf]",
      "SNI": "",
      "TlsIP": "",
      "ProxyIP": "108.139.119.85",
      "ProxyPort": "80",
      "Info": "Proxy"
    },
    {
      "Name": "VIVO T443 - 08",
      "FLAG": "vivo",
      "Payload": "GET wss://d1jcj8m073srwz.cloudfront.net HTTP/1.1[crlf]Host: d1jcj8m073srwz.cloudfront.net[crlf]Connection: Upgrade[crlf]Upgrade: websocket[crlf][crlf]",
      "SNI": "d1jcj8m073srwz.cloudfront.net",
      "TlsIP": "108.139.119.85",
      "ProxyIP": "108.139.119.85",
      "ProxyPort": "443",
      "Info": "Tlsws"
    }
